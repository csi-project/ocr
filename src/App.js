
import React from 'react';
import { useState } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.css';

import PageOCR from './component/OCR/pageOCR'
import Main from './component/Editor/Main';



function App() {

  const [selectedImage, setSelectedImage] = useState(null);

  return (
    <Router>
      <div>
        <Routes>
          <Route path="/" element={<Main setSelectedImage={setSelectedImage}  />} />
          <Route path="/OCR" element={<PageOCR selectedImage={selectedImage} setSelectedImage={setSelectedImage} />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;