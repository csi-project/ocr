import React, { useState } from "react";
import Cropper from "react-cropper";
import "cropperjs/dist/cropper.css";
import { useNavigate } from 'react-router-dom';
import BgimageDialog from "../ImageIcon/cloud-computing.png";
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import CropIcon from '@mui/icons-material/Crop';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import MuiDrawer from '@mui/material/Drawer';
import { styled } from '@mui/material/styles';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import Slider from '@mui/material/Slider';
import { useDropzone } from 'react-dropzone';
import Drawer from '@mui/material/Drawer';


import Icon from '@mdi/react';
import { mdiFlipHorizontal } from '@mdi/js';
import { mdiFlipVertical } from '@mdi/js';
import { mdiCursorMove } from '@mdi/js';
import { mdiCancel } from '@mdi/js';
import { mdiContentSave } from '@mdi/js';
import { mdiPageLayoutHeader } from '@mdi/js';
import { mdiPageLayoutBody } from '@mdi/js';
import { mdiPageLayoutFooter } from '@mdi/js';
import { mdiFileDocumentOutline } from '@mdi/js';
import WbSunnyOutlinedIcon from '@mui/icons-material/WbSunnyOutlined';
import { mdiRestart } from '@mdi/js';
import FileOpenIcon from '@mui/icons-material/FileOpen';
import FolderIcon from '@mui/icons-material/Folder';

function Main({ setSelectedImage }) {
    const fileInputRef = React.useRef(null);
    const [image, setImage] = useState(null);

    const [cropper, setCropper] = useState(null);
    const [expanded, setExpanded] = React.useState(false);
    const [rotatevalue, setRotation] = useState(0);
    const [defaultSrc, setDefaultSrc] = useState(null);
    const [openDialog, setOpenDialog] = useState(true);
    const [fileSelected, setFileSelected] = useState(false);
    const [cropDetails, setCropDetails] = useState();
    const [, setIsGrayscale] = useState(false);
    const [selectedTool, setSelectedTool] = React.useState(1);

    const handleListItemClick = (event, index) => {
      setSelectedTool(index);
    };

    const [openTooltip, setOpenTooltip] = React.useState({});
    const handleClose = (tooltipName) => {
        setOpenTooltip(prevState => ({
            ...prevState,
            [tooltipName]: false
        }));
    };

    const handleOpen = (tooltipName) => {
        setOpenTooltip((prevTooltip) => (
            prevTooltip === tooltipName ? null : tooltipName
        ));
    };

    const handleCropButtonClick = () => {
        if (typeof cropper !== "undefined") {
            cropper.setDragMode("crop");

            if (cropper.setDragMode("crop")) {
                getCropData();
            }
        }

    };

    const getCropData = () => {
        if (!cropper.setDragMode("crop")) {
            cropper.setDragMode("crop")

        } else {
            if (typeof cropper !== "undefined" && cropper.setDragMode("crop")) {
                const croppedData = cropper.getData();
                setCropDetails({
                    x: croppedData.x,
                    y: croppedData.y,
                    width: croppedData.width,
                    height: croppedData.height,
                });
                setImage(cropper.getCroppedCanvas().toDataURL());
                setSelectedImage(cropper.getCroppedCanvas().toDataURL());

            }
        }

    };


    const onChange = (e) => {
        e.preventDefault();
        let files;
        if (e.dataTransfer) {
            files = e.dataTransfer.files;
        } else if (e.target) {
            files = e.target.files;
        }

        // Ensure only image files are accepted
        const allowedTypes = ["image/jpeg", "image/png", "image/gif"]; // Add more types if needed
        if (files && files.length > 0) {
            const selectedFile = files[0];
            const fileType = selectedFile.type;

            if (allowedTypes.includes(fileType)) {
                const reader = new FileReader();
                reader.onload = () => {
                    setImage(reader.result);
                    setDefaultSrc(reader.result);
                    setSelectedImage(reader.result);
                    setIsGrayscale(false);
                };
                reader.readAsDataURL(selectedFile);
                setOpenDialog(false);
                setFileSelected(true);
            } else {
                alert("Please select a valid image file (JPEG, PNG, GIF).");
            }
        }
    };



    const handleFileButtonClick = () => {
        if (fileInputRef.current) {
            fileInputRef.current.accept = 'image/*';
            fileInputRef.current.click();
        }
    };



    const handleFlipButtonClick = () => {
        if (typeof cropper !== "undefined") {
            cropper.scaleX(-cropper.getData().scaleX); // สลับภาพแนวนอน

        }
    };

    const handleFlipVerticallyButtonClick = () => {
        if (typeof cropper !== "undefined") {
            cropper.scaleY(-cropper.getData().scaleY); // สลับภาพแนวตั้ง

        }
    };

    const handleClearButtonClick = () => {
        if (typeof cropper !== "undefined") {
            cropper.clear();

        }
    };

    const handleMoveButtonClick = () => {
        if (typeof cropper !== "undefined") {
            cropper.setDragMode("move");

        }

    };

    const handleSaveButtonClick = () => {
        if (typeof cropper !== "undefined") {
            // สร้าง Canvas เพื่อเก็บภาพที่ถูก crop และแก้ไขแล้ว
            const canvas = cropper.getCroppedCanvas();

            // ดึงข้อมูล rotate และ flip ของรูปภาพ
            const croppedData = cropper.getData();


            // สร้าง URL จาก Canvas
            const croppedDataURL = canvas.toDataURL();

            // สร้าง Object URL ของภาพที่ถูก crop และแก้ไขแล้ว
            canvas.toBlob((blob) => {
                const downloadLink = document.createElement('a');
                downloadLink.href = URL.createObjectURL(blob);
                downloadLink.download = 'edited_image.png'; // กำหนดชื่อไฟล์ที่จะเซฟ
                downloadLink.click();
            });

            // แสดงข้อมูลการแก้ไขรูปภาพ
            console.log('Cropped Data:', croppedData);
            console.log('Cropped Data URL:', croppedDataURL);
            console.log('Cropped Data:', croppedData.x);
        } else {
            alert('Please upload and crop an image before saving.');
        }
    };

    const handleChange = (panel) => (isExpanded) => {
        setExpanded(isExpanded ? panel : false);
        cropper.setDragMode("crop")
    };

    const handleReset = () => {
        setImage(defaultSrc); // กำหนดรูปภาพกลับไปยังค่า defaultSrc ที่ถูกเลือกผ่าน input
        if (cropper !== null) {
            cropper.reset(); // Reset Cropper ให้กลับไปยังตำแหน่งเริ่มต้น
        }
        setRotation(0); // รีเซ็ตค่าการหมุนกลับไปที่ 0

    };


    // Function to handle closing the dialog
    const handleCloseDialog = () => {
        if (fileSelected) {
            setOpenDialog(false);
            handleClearButtonClick();
        }
    };


    const initializeCropper = (instance) => {
        cropper.current = instance;
    };



    const selectArea = (area) => {

        if (typeof cropper !== "undefined") {

            const croppedData = cropper.getData();
            initializeCropper();
            setCropDetails((prevCropDetails) => {
                let updatedDetails = { ...prevCropDetails };
                if (!updatedDetails[area]) {
                    updatedDetails[area] = []; // สร้างโครงสร้างข้อมูลใหม่ถ้ายังไม่มีข้อมูลใน area นี้
                }
                updatedDetails[area].push({
                    x: croppedData.x,
                    y: croppedData.y,
                    width: croppedData.width,
                    height: croppedData.height,
                });
                return updatedDetails;
            });
        }


    };

    const saveCropDetailsToFile = () => {
        if (cropDetails) { // ตรวจสอบว่ามีข้อมูล cropDetails หรือไม่
            const json = JSON.stringify(cropDetails, null, 2); // แปลง JSON เป็น string พร้อมกับการจัดรูปแบบให้สวยงาม (indent 2 spaces)

            const blob = new Blob([json], { type: 'application/json' }); // สร้าง Blob object จาก JSON string

            const url = URL.createObjectURL(blob); // สร้าง URL จาก Blob object

            const link = document.createElement('a'); // สร้าง element <a> เพื่อดาวน์โหลดไฟล์
            link.href = url;
            link.download = 'data_Label.json'; // กำหนดชื่อไฟล์ที่จะเซฟ

            document.body.appendChild(link); // เพิ่ม element <a> เข้าไปใน DOM
            link.click(); // คลิกที่ element <a> เพื่อดาวน์โหลดไฟล์

            document.body.removeChild(link); // ลบ element <a> ทิ้งออกจาก DOM
            URL.revokeObjectURL(url); // ลบ URL ออกจาก memory
        } else {
            console.error('ไม่มีข้อมูล cropDetails');
        }
    };

    const clearData = () => {
        setCropDetails();
    };


    const history = useNavigate();
    const handleButtonClickGoOCRPage = () => {
        // ทำการเปลี่ยนหน้าเว็บไซต์ไปยัง "/OCR"
        history('/OCR');
    };

    const rotateImage = (event, newRotation) => {
        setRotation(newRotation);
        if (typeof cropper !== "undefined") {
            cropper.rotateTo(newRotation);
        }
    }

    const onDrop = (acceptedFiles) => {
        const file = acceptedFiles[0];
        const reader = new FileReader();

        reader.onload = () => {
            setImage(reader.result);
            setDefaultSrc(reader.result);
            setSelectedImage(reader.result);
            setIsGrayscale(false);
        };
        reader.readAsDataURL(file);
        setOpenDialog(false);
        setFileSelected(true);
    };

    const { getRootProps, getInputProps } = useDropzone({
        onDrop,
        noClick: true,
        accept: 'image/*'
    });

    return (
        <>
            <Dialog open={openDialog} onClose={handleCloseDialog}
                PaperProps={{
                    sx: {
                        maxWidth: 'lg',
                        width: "1600px",
                        height: "700px"
                    },
                }}
            >
                <div className="overflow-hidden">
                    <DialogTitle sx={{ fontSize: '40px', textAlign: 'left' }}>
                        <FolderIcon sx={{ fontSize: 40 }} />
                        Select an Image
                    </DialogTitle>
                    <DialogContent className="justify-center items-center ">
                        <div className="justify-center items-center ">
                            <div {...getRootProps()} style={{ width: '100%', height: '100%', minHeight: "550px", fontSize: '25px', border: '1px solid #ccc', borderRadius: '4px', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                <input {...getInputProps()} />
                                <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', marginTop: '10px' }}>
                                    <div className="w-72 m-0 p-0 ">
                                        <img src={BgimageDialog} className="opacity-25"></img>
                                    </div>
                                    <div className="-mt-8">
                                        <Button variant="contained" startIcon={<FileOpenIcon />} onClick={handleFileButtonClick}>Select File</Button>
                                    </div>

                                    <p style={{ marginTop: '10px', fontSize: '15px' }}>
                                        Drop files here or click to select
                                    </p>

                                </div>
                            </div>
                        </div>
                    </DialogContent>
                </div>
            </Dialog>
            <div className="flex flex-row min-h-screen">
                <div>
                    <Drawer variant="permanent" PaperProps={{ style: { backgroundColor: 'rgb(57, 57, 57)', maxWidth: '50px', overflow: 'hidden' } }}>
                        <List>
                            <ListItem disablePadding className="my-1" >
                                <Tooltip open={openTooltip === 'Move'} onClose={() => handleClose('Move')} onOpen={() => handleOpen('Move')} arrow placement="right" slotProps={{
                                    popper: {
                                        sx: {
                                            [`&.${tooltipClasses.popper}[data-popper-placement*="right"] .${tooltipClasses.tooltip}`]:
                                            {
                                                marginLeft: '50px',
                                            },
                                        },
                                    },
                                }} title="Move">
                                    <ListItemButton selected={selectedTool === 0} onClick={(event) => { handleListItemClick(event, 0); handleMoveButtonClick();}} className="max-w-10" >
                                        <ListItemIcon>
                                            <Icon style={{ color: 'rgb(210, 210, 210)' }} path={mdiCursorMove} className="max-w-5 -ml-2" />
                                        </ListItemIcon>
                                    </ListItemButton>
                                </Tooltip>
                            </ListItem>
                            <Divider sx={{
                                backgroundColor: 'white', // กำหนดสีพื้นหลังเมื่อโฮเวอร์
                            }} />
                            <ListItem disablePadding className="my-1">
                                <Tooltip open={openTooltip === 'crop'} onClose={() => handleClose('crop')} onOpen={() => handleOpen('crop')} placement="right" arrow slotProps={{
                                    popper: {
                                        sx: {
                                            [`&.${tooltipClasses.popper}[data-popper-placement*="right"] .${tooltipClasses.tooltip}`]:
                                            {
                                                marginLeft: '10px',
                                            },
                                        },
                                    },
                                }} title="Crop">
                                    <ListItemButton selected={selectedTool === 1} onClick={(event) => { handleListItemClick(event, 1); handleCropButtonClick();}} className="max-w-10">
                                        <ListItemIcon>
                                            <CropIcon style={{ color: 'rgb(210, 210, 210)' }} className="max-w-5 -ml-2" />
                                        </ListItemIcon>
                                    </ListItemButton>
                                </Tooltip>
                            </ListItem>
                            <Divider sx={{
                                backgroundColor: 'white', // กำหนดสีพื้นหลังเมื่อโฮเวอร์
                            }} />
                            <ListItem disablePadding className="my-1">
                                <Tooltip open={openTooltip === 'clear'} onClose={() => handleClose('clear')} onOpen={() => handleOpen('clear')} placement="right" arrow slotProps={{
                                    popper: {
                                        sx: {
                                            [`&.${tooltipClasses.popper}[data-popper-placement*="right"] .${tooltipClasses.tooltip}`]:
                                            {
                                                marginLeft: '50px',
                                            },
                                        },
                                    },
                                }} title="clear">
                                    <ListItemButton selected={selectedTool === 2} onClick={(event) => { handleListItemClick(event, 2); handleClearButtonClick();}} className="max-w-10">
                                        <ListItemIcon>
                                            <Icon style={{ color: 'rgb(210, 210, 210)' }} path={mdiCancel} className="max-w-5 -ml-2" />
                                        </ListItemIcon>
                                    </ListItemButton>
                                </Tooltip>
                            </ListItem>
                            <Divider sx={{
                                backgroundColor: 'white', // กำหนดสีพื้นหลังเมื่อโฮเวอร์
                            }} />
                            <ListItem disablePadding className="my-1">
                                <Tooltip open={openTooltip === 'horizontal'} onClose={() => handleClose('horizontal')} onOpen={() => handleOpen('horizontal')} placement="right" arrow slotProps={{
                                    popper: {
                                        sx: {
                                            [`&.${tooltipClasses.popper}[data-popper-placement*="right"] .${tooltipClasses.tooltip}`]:
                                            {
                                                marginLeft: '3px',
                                            },
                                        },
                                    },
                                }} title="Flip Horizontal">
                                    <ListItemButton selected={selectedTool === 3} onClick={(event) => { handleListItemClick(event, 3); handleFlipButtonClick();}} className="max-w-10">
                                        <ListItemIcon>
                                            <Icon style={{ color: 'rgb(210, 210, 210)' }} path={mdiFlipHorizontal} className="max-w-5 -ml-2" />
                                        </ListItemIcon>
                                    </ListItemButton>
                                </Tooltip>
                            </ListItem>
                            <Divider sx={{
                                backgroundColor: 'white', // กำหนดสีพื้นหลังเมื่อโฮเวอร์
                            }} />
                            <ListItem disablePadding className="my-1">
                                <Tooltip open={openTooltip === 'vertical'} onClose={() => handleClose('vertical')} onOpen={() => handleOpen('vertical')} placement="right" arrow slotProps={{
                                    popper: {
                                        sx: {
                                            [`&.${tooltipClasses.popper}[data-popper-placement*="right"] .${tooltipClasses.tooltip}`]:
                                            {
                                                marginLeft: '3px',
                                            },
                                        },
                                    },
                                }} title="Flip Vertical">
                                    <ListItemButton selected={selectedTool === 4} onClick={(event) => { handleListItemClick(event, 4); handleFlipVerticallyButtonClick();}} className="max-w-10">
                                        <ListItemIcon>
                                            <Icon style={{ color: 'rgb(210, 210, 210)' }} path={mdiFlipVertical} className="max-w-5 -ml-2" />
                                        </ListItemIcon>
                                    </ListItemButton>
                                </Tooltip>
                            </ListItem>
                            <Divider sx={{
                                backgroundColor: 'white', // กำหนดสีพื้นหลังเมื่อโฮเวอร์
                            }} />
                            <ListItem disablePadding className="my-1">
                                <Tooltip open={openTooltip === 'exposure'} onClose={() => handleClose('exposure')} onOpen={() => handleOpen('exposure')} placement="right" arrow slotProps={{
                                    popper: {
                                        sx: {
                                            [`&.${tooltipClasses.popper}[data-popper-placement*="right"] .${tooltipClasses.tooltip}`]:
                                            {
                                                marginLeft: '3px',
                                            },
                                        },
                                    },
                                }} title="Exposure">
                                    <ListItemButton selected={selectedTool === 5} onClick={(event) => { handleListItemClick(event, 5)}} className="max-w-10">
                                        <ListItemIcon>
                                            <WbSunnyOutlinedIcon style={{ color: 'rgb(210, 210, 210)' }} className="max-w-5 -ml-2" />
                                        </ListItemIcon>
                                    </ListItemButton>
                                </Tooltip>
                            </ListItem>
                            <Divider sx={{
                                backgroundColor: 'white', // กำหนดสีพื้นหลังเมื่อโฮเวอร์
                            }} />

                        </List>
                    </Drawer>
                </div>

                <div className="flex flex-col flex-1 bg-[#2c2d2e] overflow-y-hidden">
                    <div className="flex flex-row flex-1 justify-center items-center w-full h-full">
                        <Cropper
                            className="min-h-full max-h-36"
                            src={image}
                            viewMode={1}
                            zoomTo={0.5}
                            autoCropArea={0.8}
                            center
                            background={false}
                            autoCrop={false}
                            onInitialized={(instance) => {
                                setCropper(instance);

                            }}
                        >
                        </Cropper>
                    </div>
                    <div className="bg-[#393939] grid grid-cols-2">
                        <div className="justify-center items-center w-52 ml-52">
                            <div>
                                <p className="mt-1 text-[#d2d2d2]">Rotate: {rotatevalue}</p>
                            </div>
                            <Slider
                                defaultValue={0}
                                sx={{ width: 170, color: "white" }}
                                min={-45}
                                max={45}
                                step={0.1}
                                value={rotatevalue}
                                onChange={rotateImage}
                            />
                        </div>
                        <div className="justify-center items-center ml-52 m-2 grid grid-cols-2">
                            <Button
                                sx={{
                                    backgroundColor: "#666666", width: 170, color: "white", '& .MuiButton-startIcon': {
                                        marginRight: '3px'
                                    }
                                }}
                                variant="outlined"
                                startIcon={<Icon style={{ color: 'rgb(210, 210, 210)' }} path={mdiRestart} size={1} />}
                                size={"small"}
                                onClick={handleReset}
                            >
                                Reset
                            </Button>
                            <Button
                                sx={{ backgroundColor: "#5891d6", width: 170, color: "white" }}
                                variant="filled"
                                size={"small"}

                                onClick={handleCropButtonClick}
                            >Crop Image
                            </Button>
                        </div>
                    </div>
                </div>

                <div className="bg-[#393939] w-60">
                    <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')} >
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                            sx={{
                                backgroundColor: "#393939"
                            }}
                        >
                            <Icon path={mdiCursorMove} color={"#d2d2d2"} size={1} />
                            <Typography color={"#d2d2d2"}>Area</Typography>
                        </AccordionSummary>
                        <AccordionDetails sx={{ backgroundColor: "#d2d2d2" }}>
                            <Typography>
                                <ListItem disablePadding>
                                    <ListItemButton onClick={() => selectArea('Head')}>
                                        <ListItemIcon>
                                            <Icon path={mdiPageLayoutHeader} size={1} />
                                        </ListItemIcon>
                                        <ListItemText primary="Head" />
                                    </ListItemButton>
                                </ListItem>
                                <Divider sx={{
                                    backgroundColor: '#393939', // กำหนดสีพื้นหลังเมื่อโฮเวอร์
                                }} />
                                <ListItem disablePadding>
                                    <ListItemButton onClick={() => selectArea('Main')}>
                                        <ListItemIcon>
                                            <Icon path={mdiPageLayoutBody} size={1} />
                                        </ListItemIcon>
                                        <ListItemText primary="body" />
                                    </ListItemButton>
                                </ListItem>
                                <Divider sx={{
                                    backgroundColor: '#393939', // กำหนดสีพื้นหลังเมื่อโฮเวอร์
                                }} />
                                <ListItem disablePadding>
                                    <ListItemButton onClick={() => selectArea('Footer')}>
                                        <ListItemIcon>
                                            <Icon path={mdiPageLayoutFooter} size={1} />
                                        </ListItemIcon>
                                        <ListItemText primary="Footer" />
                                    </ListItemButton>
                                </ListItem>
                            </Typography>
                            <Divider sx={{
                                backgroundColor: '#393939', // กำหนดสีพื้นหลังเมื่อโฮเวอร์
                            }} />
                        </AccordionDetails>
                        <div>
                            <Card sx={{ backgroundColor: "#d2d2d2" }}>
                                <div className="bg-white overflow-auto  ">
                                    <CardContent sx={{ maxHeight: 300 }}>
                                        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                                            Label Details
                                        </Typography>
                                        <Typography variant="body2" >
                                            <pre>{JSON.stringify(cropDetails, null, 2)}</pre>
                                        </Typography>
                                    </CardContent>
                                </div>
                                <div>
                                    <CardActions>
                                        <Button size="small" onClick={saveCropDetailsToFile}>Save JSON</Button>
                                        <Button size="small" onClick={clearData}>Clear JSON</Button>
                                    </CardActions>
                                </div>
                            </Card>
                        </div>
                    </Accordion>
                    <Divider sx={{
                        backgroundColor: 'white', // กำหนดสีพื้นหลังเมื่อโฮเวอร์
                    }} />
                    <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel2a-content"
                            id="panel2a-header"
                            sx={{
                                backgroundColor: "#393939"
                            }}
                        >
                            <Typography color={"#d2d2d2"}>Training Model</Typography>
                        </AccordionSummary>
                        <AccordionDetails sx={{ backgroundColor: "#d2d2d2" }}>
                            <Typography>
                                <ListItem disablePadding>
                                    <ListItemButton onClick={handleButtonClickGoOCRPage}>
                                        <ListItemIcon>
                                            <Icon path={mdiPageLayoutHeader} size={1} />
                                        </ListItemIcon>
                                        <ListItemText primary="OCR" />
                                    </ListItemButton>
                                </ListItem>
                            </Typography>
                        </AccordionDetails>
                    </Accordion>
                    <Divider sx={{
                        backgroundColor: 'white', // กำหนดสีพื้นหลังเมื่อโฮเวอร์
                    }} />
                    <Accordion expanded={expanded === 'panel3'} onChange={handleChange('panel3')}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel3a-content"
                            id="panel3a-header"
                            sx={{
                                backgroundColor: "#393939",

                            }}
                        >
                            <Typography color={"#d2d2d2"}>File Management</Typography>
                        </AccordionSummary>
                        <AccordionDetails sx={{ backgroundColor: "#d2d2d2" }}>
                            <Typography>
                                <List>
                                    <ListItem disablePadding>
                                        <ListItemButton onClick={handleFileButtonClick}>
                                            <ListItemIcon>
                                                <Icon path={mdiFileDocumentOutline} size={1} />
                                            </ListItemIcon>
                                            <ListItemText primary="File" />
                                        </ListItemButton>
                                        <input
                                            type="file"
                                            id="fileInput"
                                            ref={fileInputRef}
                                            style={{ display: 'none' }}
                                            onChange={onChange}
                                        />
                                    </ListItem>
                                    <Divider sx={{
                                        backgroundColor: '#393939', // กำหนดสีพื้นหลังเมื่อโฮเวอร์
                                    }} />
                                    <ListItem disablePadding>
                                        <ListItemButton onClick={handleSaveButtonClick}>
                                            <ListItemIcon>
                                                <Icon path={mdiContentSave} size={1} />
                                            </ListItemIcon>
                                            <ListItemText primary="Save Image" />
                                        </ListItemButton>
                                    </ListItem>
                                </List>
                            </Typography>
                        </AccordionDetails>
                    </Accordion>
                    <Divider sx={{
                        backgroundColor: 'white', // กำหนดสีพื้นหลังเมื่อโฮเวอร์
                    }} />
                </div>
            </div >
        </>
    );
}

export default Main;