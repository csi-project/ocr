import React, { useCallback, useEffect, useState } from 'react';
import { createWorker } from 'tesseract.js';
import { useNavigate } from 'react-router-dom';

import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import ReceiptLongIcon from '@mui/icons-material/ReceiptLong';
import { Button } from '@mui/material';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';

function PageOCR({ selectedImage, setSelectedImage }) {
  const fileInputRef = React.useRef(null);
  const [textResult, setTextResult] = useState("");
  const [, setLoading] = useState(false);

  const convertImageToText = useCallback(async () => {
    if (!selectedImage) return;
    setLoading(true);
    // const worker = await createWorker('tha');
    const worker = await createWorker(['tha','eng'],
      {
        options: {
          langPath: 'src/ocr-font/tha.traineddata',
        }
      }
    );
    const { data } = await worker.recognize(selectedImage);
    setTextResult(data.text);
    setLoading(false);

  }, [selectedImage]);

  useEffect(() => {
    convertImageToText();
  }, [selectedImage, convertImageToText]);

  const onChange = (e) => {
    e.preventDefault();
    let files;
    if (e.dataTransfer) {
      files = e.dataTransfer.files;
    } else if (e.target) {
      files = e.target.files;
    }

    // Ensure only image files are accepted
    const allowedTypes = ["image/jpeg", "image/png", "image/gif"]; // Add more types if needed
    if (files && files.length > 0) {
      const selectedFile = files[0];
      const fileType = selectedFile.type;

      if (allowedTypes.includes(fileType)) {
        const reader = new FileReader();
        reader.onload = () => {
          setSelectedImage(reader.result);
        };
        reader.readAsDataURL(selectedFile);
      } else {
        alert("Please select a valid image file (JPEG, PNG, GIF).");
      }
    }
  };

  const handleFileButtonClick = () => {
    if (fileInputRef.current) {
      fileInputRef.current.accept = 'image/*';
      fileInputRef.current.click();
    }
  };

  const saveTextToFileAsJSON = () => {
    if (textResult) {
      const jsonData = {
        textOCR: textResult,
        timestamp: new Date().toISOString(),
      };

      const jsonString = JSON.stringify(jsonData, null, 2);

      const blob = new Blob([jsonString], { type: 'application/json' });

      // สร้าง URL จาก Blob
      const url = URL.createObjectURL(blob);

      // สร้างลิงค์สำหรับดาวน์โหลด
      const link = document.createElement('a');
      link.href = url;
      link.download = 'textOCR.json';

      // เพิ่มลิงค์ไปยัง DOM
      document.body.appendChild(link);

      // คลิกที่ลิงค์
      link.click();

      // ลบลิงค์ออกจาก DOM
      document.body.removeChild(link);

      // ลบ URL จากหน่วยความจำ
      URL.revokeObjectURL(url);
    } else {
      console.error('ไม่มีข้อมูล textResult');
    }
  };

  const history = useNavigate();
  const handleButtonClick = () => {
    // ทำการเปลี่ยนหน้าเว็บไซต์ไปยัง "/OCR"
    history('/');
  };

  return (
    <div className='App flex flex-row'>
      <div className=''>
        <AppBar sx={{ backgroundColor: 'black' }}>
          <Toolbar>
            <div>
              <Button onClick={handleButtonClick}>
                <ArrowBackIosIcon sx={{ color: 'white' }}></ArrowBackIosIcon>
              </Button>
            </div>
            <div className='flex flex-row flex-1'>
              <ReceiptLongIcon sx={{ display: { xs: 'none', md: 'flex' }, mr: 1 }} ></ReceiptLongIcon>
              <Typography variant="h6" component="div">
                ImageToText
              </Typography>
            </div>
            <div className='flex flex-row'>
              <div className='ml-5'>
                <input
                  type="file"
                  id="fileInput"
                  ref={fileInputRef}
                  style={{ display: 'none' }}
                  onChange={onChange}
                />
                <Button sx={{ color: 'white', display: 'block' }} onClick={handleFileButtonClick}>
                  File
                </Button>
              </div>
              <div className='ml-1'>
                <Button sx={{ color: 'white', display: 'block' }} onClick={saveTextToFileAsJSON}>
                  Save
                </Button>

              </div>
            </div>
          </Toolbar>
        </AppBar>
      </div>
      <div className="result">
        {selectedImage && (
          <div className="box-image">
            <img src={selectedImage} alt="thumb" />
          </div>
        )}
        {textResult && (
          <div className="box-p">
            <p>{textResult}</p>
          </div>
        )}
      </div>
    </div>

  );
}

export default PageOCR;
